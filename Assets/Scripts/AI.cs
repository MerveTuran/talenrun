﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class AI : MonoBehaviour
{
    private int range;
    private float speed;
    private bool isThereAnyThing = false;
    public GameObject target;
    private Button yourButton;
    private float rotationSpeed;
    private RaycastHit hit;
    void Start()
    {
        range = 1;
        speed = 1;
        rotationSpeed = 300;
        yourButton = GetComponent<Button>();
        //gameObject.GetComponent<AI>().enabled = false;
        enabled = false;
        yourButton = GameObject.FindWithTag("Play").GetComponent<Button>();
        yourButton.onClick.AddListener(PlayClick);
    }
    void PlayClick()
    {
        enabled = true;
    }
    void FixedUpdate()
    {
        if (!isThereAnyThing)
        {
            Vector3 relativePos = target.transform.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.fixedDeltaTime);
        }

        transform.Translate(Vector3.forward * Time.fixedDeltaTime * speed);

        Transform leftRay = transform;
        Transform rightRay = transform;

        if (Physics.Raycast(rightRay.position - (transform.right), transform.forward, out hit, range))
        {
            if (hit.collider.gameObject.CompareTag("Obstacle"))
            {
                isThereAnyThing = true;
                transform.Rotate(Vector3.up * Time.fixedDeltaTime * rotationSpeed);
            }
        }
        if (Physics.Raycast(leftRay.position + (transform.right), transform.forward, out hit, range))
        {
            if (hit.collider.gameObject.CompareTag("Obstacle"))
            {
                isThereAnyThing = true;
                transform.Rotate(Vector3.down * Time.fixedDeltaTime * rotationSpeed);
            }
        }

        if (Physics.Raycast(transform.position - (transform.forward * 3), transform.right, out hit,500) ||
         Physics.Raycast(transform.position - (transform.forward * 3), -transform.right, out hit, 500))
        {
            if (hit.collider.gameObject.CompareTag("Obstacle"))
            {
                isThereAnyThing = false;
            }
        }

        Debug.DrawRay(transform.position + (transform.right), transform.forward, Color.red);
        Debug.DrawRay(transform.position - (transform.right), transform.forward, Color.red);
        Debug.DrawRay(transform.position - (transform.forward), -transform.right, Color.yellow);
        Debug.DrawRay(transform.position - (transform.forward), transform.right, Color.yellow);

    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.collider.tag == ("Obstacle"))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        }
        if (collision.collider.tag == ("Water"))
        {
            Destroy(this);
        }
    }
}
