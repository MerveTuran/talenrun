﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ranking : MonoBehaviour
{
    public Vector3 relativePosition = new Vector3();
    public Transform Player;
    public Transform[] Target;
    private Button yourButton;
    void Start()
    {
        enabled = false;
        yourButton = GameObject.FindWithTag("Play").GetComponent<Button>();
        yourButton.onClick.AddListener(PlayClick);
    }
    void PlayClick()
    {
        enabled = true;
    }
    void Update()
    {
        int numberOfFrontAi = 0;
        for (int i = 0; i < Target.Length; i++)
        {
            Vector3 relativePosition = transform.InverseTransformPoint(Target[i].transform.position);
            if (relativePosition.z < 0)
            {
                numberOfFrontAi++;
            }

            int ranking = (11 - numberOfFrontAi);
            GameObject.Find("RankingText").GetComponentInChildren<Text>().text = ranking.ToString();
        }
    }
}
