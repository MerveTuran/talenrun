﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Restart : MonoBehaviour
{
	public GameObject[] canvasObject;
	public Button yourButton;
	public Button button;
	void Start()
	{
		Button btn = yourButton.GetComponent<Button>();
		Button btn2 = button.GetComponent<Button>();
		btn.onClick.AddListener(RestarClick);
		btn2.onClick.AddListener(ExitClick);
	}

	void RestarClick()
	{
		string currentSceneName = SceneManager.GetActiveScene().name;
		SceneManager.LoadScene(currentSceneName);

	}
	void ExitClick()
	{
		Application.Quit();

	}
}
