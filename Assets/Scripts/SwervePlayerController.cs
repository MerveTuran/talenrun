﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SwervePlayerController : MonoBehaviour
{
    private float _lastFrameFingerPositionX;
    private float _moveFactorX;
    public float MoveFactorX => _moveFactorX;
    Animator animator;
    public float movementSpeed = 0.60f;
    private Button yourButton;

    void Start()
    {
        yourButton = GetComponent<Button>();
        enabled = false;
        yourButton = GameObject.FindWithTag("Play").GetComponent<Button>();
        yourButton.onClick.AddListener(PlayClick);
        animator = GetComponent<Animator>();
    }
    void PlayClick()
    {
        enabled = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            animator.SetBool("isRunning", true);
            _lastFrameFingerPositionX = Input.mousePosition.x;
            //float angle = Mathf.Atan2(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal")) * Mathf.Rad2Deg;
            //transform.GetChild(0).rotation = Quaternion.Euler(new Vector3(0, 0, angle));

        }
        else if (Input.GetMouseButton(0))
        {
            _moveFactorX = Input.mousePosition.x - _lastFrameFingerPositionX;
            _lastFrameFingerPositionX = Input.mousePosition.x;
            transform.position += Vector3.forward * Time.deltaTime * movementSpeed;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            animator.SetBool("isRunning", false);
            _moveFactorX = 0f;
        }

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Obstacle")
        {
            string currentSceneName = SceneManager.GetActiveScene().name;
            SceneManager.LoadScene(currentSceneName);

        }
    }
}
