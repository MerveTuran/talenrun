﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayButton : MonoBehaviour
{
	public GameObject[] canvasObject;
	public Button yourButton;
	void Start()
	{
		Button btn = yourButton.GetComponent<Button>();
		btn.onClick.AddListener(PlayClick);
	}

	void PlayClick()
	{
		canvasObject[0].SetActive(true);
		canvasObject[1].SetActive(false);
		//GameObject.FindWithTag("MainCanvas").SetActive(false);
		//GameObject.FindWithTag("RankingCanvas").GetComponent<GameObject>().SetActive(true);
		//string currentSceneName = SceneManager.GetActiveScene().name;
		//SceneManager.LoadScene(currentSceneName);
	}
}
