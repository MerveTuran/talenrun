﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwerveMovement : MonoBehaviour
{
    private SwervePlayerController _swervePlayerController;
    [SerializeField] private float swerveSpeed = 0.60f;
    [SerializeField] private float maxSwerveAmount = 1f;
    [SerializeField] private Rigidbody _rigidbody;

    private void Awake()
    {
        _swervePlayerController = GetComponent<SwervePlayerController>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        float swerveAmount = Time.deltaTime * swerveSpeed * _swervePlayerController.MoveFactorX;
        swerveAmount = Mathf.Clamp(swerveAmount, -maxSwerveAmount, maxSwerveAmount);
        transform.Translate(swerveAmount, 0, 0);
    }
}
