﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateStick : MonoBehaviour
{
    float Lspeed = 50f;

    private void OnCollisionStay(Collision collision)
    {
        if (collision.collider.tag == ("Player"))
        {
            collision.transform.Translate(Vector3.forward * Lspeed * Time.deltaTime);
        }
    }
}
