﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateRight : MonoBehaviour
{
    float speed = 30;
    float Lspeed = 0.30f;

    void Start()
    {

    }

    void FixedUpdate()
    {
        transform.Rotate(Vector3.back * speed * Time.deltaTime);
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.collider.tag == ("Player"))
        {
            collision.transform.Translate(Vector3.right * Lspeed * Time.deltaTime);
        }
    }
}
