﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateDonuts : MonoBehaviour
{
    public Vector3 rotationDirection;
    public float durationTime;
    private float smooth;

    void Start()
    {

    }

    void Update()
    {
        smooth = Time.deltaTime * durationTime;
        transform.Rotate(rotationDirection * smooth);
    }
}
