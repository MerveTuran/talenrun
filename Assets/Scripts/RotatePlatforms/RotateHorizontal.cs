﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateHorizontal : MonoBehaviour
{
    public Vector3 rotationDirection;
    public float durationTime;
    private float smooth;
    public float movementSpeed = 50.0f;

    void Start()
    {
        StartCoroutine(waiter());
    }
    IEnumerator waiter()
    {
        //Rotate 90 deg
        transform.Rotate(new Vector3(90, 0, 0), Space.World);

        //Wait for 4 seconds
        yield return new WaitForSeconds(4);

        //Rotate 40 deg
        transform.Rotate(new Vector3(0, 40, 0), Space.World);

        //Wait for 2 seconds
        yield return new WaitForSeconds(2);

        //Rotate 20 deg
        transform.Rotate(new Vector3(20, 0, 0), Space.World);
    }

    //IEnumerator waiter()
    //{
    //    float counter = 0;
    //    float waitTime = 4;
    //    while (counter < waitTime)
    //    {
    //        counter += Time.deltaTime;
    //        smooth = Time.deltaTime * durationTime;
    //        transform.Rotate(rotationDirection * smooth);
    //        transform.position += Vector3.right * Time.deltaTime * movementSpeed;

    //        yield return null;
    //    }
    //    smooth = Time.deltaTime * durationTime;
    //    transform.Rotate(rotationDirection * smooth);
    //    transform.position += Vector3.left * Time.deltaTime * movementSpeed;
    //}
}
