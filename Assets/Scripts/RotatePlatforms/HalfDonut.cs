﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HalfDonut : MonoBehaviour
{
    public float min = 0f;
    public float max = 0f; 

    void Start()
    {

    }

    void Update()
    {
        transform.position = new Vector3(Mathf.PingPong(Time.time * 1, max - min) + min, transform.position.y, transform.position.z);
    }
}
