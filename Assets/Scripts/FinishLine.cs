﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishLine : MonoBehaviour
{
    public GameObject[] girl;
    private Animation animation;
    void OnTriggerEnter(Collider col)
    {

        if (col.tag == "Player")
        {
            girl[14].SetActive(true);
            girl[13].SetActive(false);
            girl[11].SetActive(true);
            girl[12].GetComponent<SwervePlayerController>().enabled = false;
            girl[12].GetComponent<Ranking>().enabled = false;
            girl[0].SetActive(true);
            animation["WallCamera"].wrapMode = WrapMode.Once;
            animation.Play("WallCamera");
            
        }

            string index = col.tag.Replace("Opponent","");
             int girli = int.Parse(index);
             girl[girli].SetActive(false); 
    }

}
