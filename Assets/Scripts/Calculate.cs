﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calculate : MonoBehaviour
{
    public Texture2D sourceTex;
    public GameObject gameObject;

    void Start()
    {
        RectTransform rt = (RectTransform)gameObject.transform;

        int x = Mathf.FloorToInt(gameObject.transform.position.x);
        int y = Mathf.FloorToInt(gameObject.transform.position.y);
        int width = Mathf.FloorToInt(rt.rect.width);
        int height = Mathf.FloorToInt(rt.rect.height);

        Color[] pix = sourceTex.GetPixels(x, y, width, height);
        Debug.Log(pix);
        Debug.Log(x);
        Debug.Log(y);
        //Texture2D destTex = new Texture2D(width, height);
        //destTex.SetPixels(pix);
        //destTex.Apply();

        // Set the current object's texture to show the
        //// extracted rectangle.
        //GetComponent<Renderer>().material.mainTexture = destTex;
    }
}
